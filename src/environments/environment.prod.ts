export const environment = {
  production: true,
  isMockEnabled: true, // You have to switch this, when your real back-end is done
  codigoSistema: '0001',
  timeToRefreshToken: 30000,

  jwt: {
    audience: 'DefaultAudience',
    issuer: 'DefaultIssuer'
  },

  /*WEB SERVICES*/
  api: {
    WS_ECOMMERCE: {
      url: 'http://localhost:50074/api',
      basicAuthorization: {
        username: 'VnSDApp',
        password: '975318642'
      }
    }
  },
  firebaseProject: {
    apiKey: "AIzaSyBBfZnPFrITkbVLJ0Fip0qwyTLTymToAEM",
    authDomain: "sanciones-483d5.firebaseapp.com",
    databaseURL: "https://sanciones-483d5.firebaseio.com",
    projectId: "sanciones-483d5",
    storageBucket: "sanciones-483d5.appspot.com",
    messagingSenderId: "922715428675",
    appId: "1:922715428675:web:6d3109c6b8e7d28e4681f6",
    measurementId: "G-3W90M5YKCH"
  }, 
};