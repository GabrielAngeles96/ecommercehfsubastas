importScripts('https://www.gstatic.com/firebasejs/7.23.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.23.0/firebase-messaging.js');


firebase.initializeApp({
    apiKey: "AIzaSyBBfZnPFrITkbVLJ0Fip0qwyTLTymToAEM",
    authDomain: "sanciones-483d5.firebaseapp.com",
    databaseURL: "https://sanciones-483d5.firebaseio.com",
    projectId: "sanciones-483d5",
    storageBucket: "sanciones-483d5.appspot.com",
    messagingSenderId: "922715428675",
    appId: "1:922715428675:web:6d3109c6b8e7d28e4681f6",
    measurementId: "G-3W90M5YKCH"
})

const messaging = firebase.messaging();

messaging.onBackgroundMessage(function (payload) {
    // console.log(payload);
    console.log('listen backgroud');
  // Customize notification here
  const notificationTitle = 'NUEVA OFERTA';
  const notificationOptions = {
    body: 'Acaban de hacer una nueva oferta por la subasta en la que estás participando actualmente.',
    icon: '/firebase-logo.png'
  };

  self.registration.showNotification(notificationTitle,
    notificationOptions);
});