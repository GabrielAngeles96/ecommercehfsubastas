export enum ApiEnum {
    WS_IT,
    WS_CORE,
    WS_ECOMMERCE,
    WS_VISANET
}