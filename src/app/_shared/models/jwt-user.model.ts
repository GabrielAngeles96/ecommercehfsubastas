export interface JwtUserModel {
    IdUsuario: number,
    Nombres: string,
    Apellidos: string,
    ApellidoPaterno: string,
    /*Nombres: string,
    ApellidoPaterno: string,
    ApellidoMaterno: string,*/
    Rol: string,
    Login: string,
    //Email: string,
    Sociedad: string,
    DescSociedad: string,
    //Sociedades: string,
    IdSociedad: number,
    IdEmpresa: number,
    NroComprobante: number,
    Carrito: number,
    TipoUsuario: boolean,
    DiasRegistro: number,
    Email: string,
    UserVNId: string,
    Bloqueado: boolean
}