import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {
	@HostBinding('class') classes: string = 'm-grid m-grid--hor m-grid--root m-page';
	@Input() errorType: string;

	constructor(private route: ActivatedRoute) {
	}

	ngOnInit() {
		this.errorType = this.route.snapshot.paramMap.get('type');
		if (!this.errorType) {
			this.errorType = '404';//Default 404
		}
	}
}
