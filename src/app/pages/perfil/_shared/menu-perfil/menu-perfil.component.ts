import { Component, OnInit, Input } from '@angular/core';
import { JwtService } from 'app/_shared/services/jwt.service';
import { JwtUserModel } from 'app/_shared/models/jwt-user.model';

@Component({
  selector: 'app-menu-perfil',
  templateUrl: './menu-perfil.component.html',
  styleUrls: ['./menu-perfil.component.scss']
})
export class MenuPerfilComponent implements OnInit {
  @Input() opcion: number;
  jwtUser: JwtUserModel;
  name_user: string;

  constructor(
    public jwtService: JwtService,
  ) { }

  ngOnInit() {
    this.jwtUser = this.jwtService.getUser();
    this.name_user = this.jwtUser.Nombres;
  }
}
