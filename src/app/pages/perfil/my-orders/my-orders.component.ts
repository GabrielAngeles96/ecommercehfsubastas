import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ResumenDialogComponent } from './resumen-dialog/resumen-dialog.component';
import { PerfilService } from '../_core/services/perfil.service';
import { JwtService } from 'app/_shared/services/jwt.service';
import { JwtUserModel } from 'app/_shared/models/jwt-user.model';
import { Meta } from '@angular/platform-browser';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.scss']
})
export class MyOrdersComponent implements OnInit {
  modalRef: BsModalRef;
  jwtUser: JwtUserModel;
  arrayOrders: any[] = [];

  page = 1;
  pageSize = 10;

  constructor(
    private modalService: BsModalService,
    private perfilService: PerfilService,
    public jwtService: JwtService,
    private metaTagService: Meta,
    private titleService: Title,
  ) { }

  ngOnInit() {
    this.setTagsAndTitle()
    this.getOrders();
    this.jwtUser = this.jwtService.getUser();
  }

  setTagsAndTitle() {
    this.titleService.setTitle('SUBASTAS HF | MIS PEDIDOS')
      
    //this.metaTagService.updateTag({ name: 'keywords', content: 'Angular SEO Integration, Music CRUD, Angular Universal' });
    this.metaTagService.updateTag({ name: 'robots', content: 'INDEX, FOLLOW' });
    this.metaTagService.updateTag({ name: 'description', content: 'Ingresa y descubre todas las promociones en alimentos frescos y en conserva. ¡Ingresa y haz tu pedido online!' });

    this.metaTagService.updateTag({ property: 'og:title', content: 'SUBASTAS HF' });
    this.metaTagService.updateTag({ property: 'og:type', content: 'website' });
    //this.metaTagService.updateTag({ property: 'og:image', content: 'https://www.bembos.com.pe/bembos/images/logo_1200_x_630.png?v=2' });
    this.metaTagService.updateTag({ property: 'og:url', content: 'https://hortifrut.com/' });
    this.metaTagService.updateTag({ property: 'og:description', content: 'Ingresa y descubre todas las promociones en alimentos frescos y en conserva. ¡Ingresa y haz tu pedido online!' });
  }

  getOrders() {
    this.perfilService.getOrders().subscribe(
      (data: any) => {
        this.arrayOrders = data;
      }
    )
  }

  openResumen(order) {
    var titulo = "Resumen de Pedido";

    const initialState = {
      title: titulo,
      objPedido: order
    };

    this.modalRef = this.modalService.show(ResumenDialogComponent, { initialState, ignoreBackdropClick: true, keyboard: false, class: 'modal-resumen' });
    this.modalRef.content.onClose.subscribe(result => {

      if (result) {
        //this.getMainList();
      }
    });
  }

  formatNumber(n: number){
    var val = n.toFixed(2);
    var parts = val.toString().split(".");
    var num = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");

    return num;
  }
}
