import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-resumen-dialog',
  templateUrl: './resumen-dialog.component.html',
  styleUrls: ['./resumen-dialog.component.scss']
})
export class ResumenDialogComponent implements OnInit {
  public onClose: Subject<boolean>;
  
  title: string;

  objPedido;

  subTotal = 0;

  constructor(
    public bsModalRef: BsModalRef,
  ) { }

  ngOnInit() {
    this.onClose = new Subject();

    this.objPedido.DPedido.forEach((item) => {
      this.subTotal += item.PrecioUnitario * item.Cantidad;
    });
  }

  closeModal() {
    this.onClose.next(true);
    this.bsModalRef.hide();
  }

  formatNumber(n: number){
    var val = n.toFixed(2);
    var parts = val.toString().split(".");
    var num = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");

    return num;
  }
}
