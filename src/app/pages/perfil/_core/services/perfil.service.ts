import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { HeaderBasicAuthorizationService } from '../../../../_shared/services/header-basic-authorization.service';
import { ApiEnum } from '../../../../_shared/enums/api.enum';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PerfilService {

  constructor(
    private http: HttpClient,
    private headerBasicAuthorization: HeaderBasicAuthorizationService
  ) { }

  public updateCliente(Email, RazonSocial, RUC, Telefono) {
		return this.http.post(`${environment.api.WS_ECOMMERCE.url}/Cliente/UpdateClienteWeb`,
      { prmstrEmail: Email, prmstrRazonSocial: RazonSocial, prmstrRUC: RUC, prmstrTelefono: Telefono },		  
		  { headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
		 .pipe(map(data => data));
  }

  public changePass(Login, Pass, NewPass, RePass) {
        return this.http.post(`${environment.api.WS_ECOMMERCE.url}/Seguridad/ChangePasswordWeb`,
        { prmstrLogin: Login, prmstrLastPass: Pass, prmstrNewPass: NewPass, prmstrNewPassConfirmation: RePass },		  
        { headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
        .pipe(map(data => data));
  }

  public getCliente() {
		return this.http.get(`${environment.api.WS_ECOMMERCE.url}/Cliente/GetCliente`,  
		{ headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
	    .pipe(map(data => data));
  }

  public getOrders() {
    return this.http.get(`${environment.api.WS_ECOMMERCE.url}/Subasta/GetSubastasByCliente`,	  
    { headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
      .pipe(map(data => data));
  }
}