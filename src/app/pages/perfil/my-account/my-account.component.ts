import { Component, OnInit, Input } from '@angular/core';
import { JwtService } from 'app/_shared/services/jwt.service';
import { JwtUserModel } from 'app/_shared/models/jwt-user.model';
import { Meta } from '@angular/platform-browser';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.scss']
})
export class MyAccountComponent implements OnInit {
  @Input() opcion: number;
  jwtUser: JwtUserModel;
  name_user: string;
  typeUser: any;

  constructor(
    public jwtService: JwtService,
    private metaTagService: Meta,
    private titleService: Title,
  ) { }

  ngOnInit() {
    this.setTagsAndTitle();
    this.jwtUser = this.jwtService.getUser();
    this.name_user = this.jwtUser.Nombres;
  }

  setTagsAndTitle() {
    this.titleService.setTitle('SUBASTAS HF | MI CUENTA')
      
    //this.metaTagService.updateTag({ name: 'keywords', content: 'Angular SEO Integration, Music CRUD, Angular Universal' });
    this.metaTagService.updateTag({ name: 'robots', content: 'INDEX, FOLLOW' });
    this.metaTagService.updateTag({ name: 'description', content: 'Ingresa y descubre todas las promociones en alimentos frescos y en conserva. ¡Ingresa y haz tu pedido online!' });

    this.metaTagService.updateTag({ property: 'og:title', content: 'SUBASTAS HF' });
    this.metaTagService.updateTag({ property: 'og:type', content: 'website' });
    //this.metaTagService.updateTag({ property: 'og:image', content: 'https://www.bembos.com.pe/bembos/images/logo_1200_x_630.png?v=2' });
    this.metaTagService.updateTag({ property: 'og:url', content: 'https://hortifrut.com/' });
    this.metaTagService.updateTag({ property: 'og:description', content: 'Ingresa y descubre todas las promociones en alimentos frescos y en conserva. ¡Ingresa y haz tu pedido online!' });
  }
}
