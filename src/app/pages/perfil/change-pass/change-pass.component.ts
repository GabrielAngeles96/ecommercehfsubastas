import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { JwtUserModel } from 'app/_shared/models/jwt-user.model';
import { JwtService } from 'app/_shared/services/jwt.service';
import { PerfilService } from '../_core/services/perfil.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrManager } from 'ng6-toastr-notifications';
import { Meta } from '@angular/platform-browser';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-change-pass',
  templateUrl: './change-pass.component.html',
  styleUrls: ['./change-pass.component.scss']
})
export class ChangePassComponent implements OnInit {
  form: FormGroup;

  jwtUser: JwtUserModel;

  typeUser: any;

  passLast: boolean = false;
  passNew: boolean = false;
  passReNew: boolean = false;
  showErrorRePass: boolean = false;

  constructor(
    private _formBuilder: FormBuilder,
    public jwtService: JwtService,
    private change_ref: ChangeDetectorRef,
    private perfilService: PerfilService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrManager,
    private metaTagService: Meta,
    private titleService: Title,
  ) { }

  ngOnInit() {
    this.setTagsAndTitle();
    this.createForm();

    this.jwtUser = this.jwtService.getUser();
    this.typeUser = this.jwtUser.TipoUsuario;

    var controls = this.form.controls;

    if (this.typeUser) {
      controls.Password.disable();
      controls.NewPassword.disable();
      controls.ReNewPassword.disable();
    }
  }

  setTagsAndTitle() {
    this.titleService.setTitle('SUBASTAS HF | CAMBIAR CONTRASEÑA')
      
    //this.metaTagService.updateTag({ name: 'keywords', content: 'Angular SEO Integration, Music CRUD, Angular Universal' });
    this.metaTagService.updateTag({ name: 'robots', content: 'INDEX, FOLLOW' });
    this.metaTagService.updateTag({ name: 'description', content: 'Ingresa y descubre todas las promociones en alimentos frescos y en conserva. ¡Ingresa y haz tu pedido online!' });

    this.metaTagService.updateTag({ property: 'og:title', content: 'SUBASTAS HF' });
    this.metaTagService.updateTag({ property: 'og:type', content: 'website' });
    //this.metaTagService.updateTag({ property: 'og:image', content: 'https://www.bembos.com.pe/bembos/images/logo_1200_x_630.png?v=2' });
    this.metaTagService.updateTag({ property: 'og:url', content: 'https://hortifrut.com/' });
    this.metaTagService.updateTag({ property: 'og:description', content: 'Ingresa y descubre todas las promociones en alimentos frescos y en conserva. ¡Ingresa y haz tu pedido online!' });
  }

  createForm() {
    this.form = this._formBuilder.group({
      Password: [null, [Validators.required, Validators.minLength(8)]],
      NewPassword: [null, [Validators.required, Validators.minLength(8)]],
      ReNewPassword: [null, [Validators.required, Validators.minLength(8)]]
    });
  }

  onSubmit(){
    this.spinner.show();
    const _controls = this.form.controls;

		if (this.form.invalid) {
			Object.keys(_controls).forEach(controlName =>
        _controls[controlName].markAsTouched()
      );
      this.spinner.hide();
    } else if (_controls.NewPassword.value != _controls.ReNewPassword.value) {
      Object.keys(_controls).forEach(controlName =>
        _controls[controlName].markAsTouched()
      );
      this.showErrorRePass = true;
      this.spinner.hide();
    } else {
        this.perfilService.changePass(this.jwtUser.Login, _controls.Password.value,
          _controls.NewPassword.value, _controls.ReNewPassword.value).subscribe(
          (data: any) => {
            if (data.Ok) {
              this.change_ref.markForCheck();
              this.toastr.successToastr(data.Message + '.', 'Éxito!', {
                toastTimeout: 2000,
                showCloseButton: true,
                animate: 'fade',
                progressBar: true
              });

              this.spinner.hide();

              this.form.reset();
            } else {
              this.toastr.warningToastr(data.Message + '.', 'Advertencia!', {
                toastTimeout: 2000,
                showCloseButton: true,
                animate: 'fade',
                progressBar: true
              });

              this.spinner.hide();
            }
          }, ( errorServicio ) => {
            console.log(errorServicio);
            this.change_ref.markForCheck();
            this.spinner.hide();
          }
        );
    }
  }

  changeIconPass(control) {
    if (control == 1) {
      this.passLast = !this.passLast;
    } else if (control == 2) {
      this.passNew = !this.passNew;
    } else {
      this.passReNew = !this.passReNew;
    }
  }

  changeShowError() {
    this.showErrorRePass = false;
  }
}
