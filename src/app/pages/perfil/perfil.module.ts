import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerfilRoutingModule } from './perfil-routing.module';
import { CuentaComponent } from './cuenta/cuenta.component';
import { MenuPerfilComponent } from './_shared/menu-perfil/menu-perfil.component';
import { ChangePassComponent } from './change-pass/change-pass.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { ResumenDialogComponent } from './my-orders/resumen-dialog/resumen-dialog.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { MyAccountComponent } from './my-account/my-account.component';
import { HelpComponent } from './help/help.component';

@NgModule({
  declarations: [
    CuentaComponent,
    MenuPerfilComponent,
    ChangePassComponent,
    MyOrdersComponent,
    ResumenDialogComponent,
    MyAccountComponent,
    HelpComponent
  ],
  imports: [
    CommonModule,
    PerfilRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot()
  ],
  entryComponents: [
    ResumenDialogComponent
  ]
})
export class PerfilModule { }
