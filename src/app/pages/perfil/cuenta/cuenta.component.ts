import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { defineLocale } from "ngx-bootstrap/chronos";
import { esLocale } from "ngx-bootstrap/locale";
import { PerfilService } from '../_core/services/perfil.service';
import { JwtService } from 'app/_shared/services/jwt.service';
import { JwtUserModel } from 'app/_shared/models/jwt-user.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrManager } from 'ng6-toastr-notifications';
import { DatePipe } from '@angular/common';
import { Meta } from '@angular/platform-browser';
import { Title } from '@angular/platform-browser';

defineLocale("es", esLocale);

@Component({
  selector: 'app-cuenta',
  templateUrl: './cuenta.component.html',
  styleUrls: ['./cuenta.component.scss'],
  providers: [DatePipe]
})
export class CuentaComponent implements OnInit {
  form: FormGroup;
  jwtUser: JwtUserModel;
  arraryCliente: any[];

  showErrorDate: boolean = false;
  today = new Date();

  constructor(
    private _formBuilder: FormBuilder,
    private localeService: BsLocaleService,
    private perfilService: PerfilService,
    public jwtService: JwtService,
    private change_ref: ChangeDetectorRef,
    private spinner: NgxSpinnerService,
    private toastr: ToastrManager,
    public datePipe: DatePipe,
    private metaTagService: Meta,
    private titleService: Title,
  ) { 
    this.localeService.use('es');
  }

  ngOnInit() {
    this.setTagsAndTitle()
    this.createForm();
    this.getCliente();
    this.jwtUser = this.jwtService.getUser();
  }

  setTagsAndTitle() {
    this.titleService.setTitle('SUBASTAS HF | DATOS PERSONALES')
      
    //this.metaTagService.updateTag({ name: 'keywords', content: 'Angular SEO Integration, Music CRUD, Angular Universal' });
    this.metaTagService.updateTag({ name: 'robots', content: 'INDEX, FOLLOW' });
    this.metaTagService.updateTag({ name: 'description', content: 'Ingresa y descubre todas las promociones en alimentos frescos y en conserva. ¡Ingresa y haz tu pedido online!' });

    this.metaTagService.updateTag({ property: 'og:title', content: 'SUBASTAS HF' });
    this.metaTagService.updateTag({ property: 'og:type', content: 'website' });
    //this.metaTagService.updateTag({ property: 'og:image', content: 'https://www.bembos.com.pe/bembos/images/logo_1200_x_630.png?v=2' });
    this.metaTagService.updateTag({ property: 'og:url', content: 'https://hortifrut.com/' });
    this.metaTagService.updateTag({ property: 'og:description', content: 'Ingresa y descubre todas las promociones en alimentos frescos y en conserva. ¡Ingresa y haz tu pedido online!' });
  }

  createForm() {
    this.form = this._formBuilder.group({
      TipoDocumento: [1, [Validators.required]],
      NroDocumento: [null, [Validators.required, Validators.minLength(10), Validators.maxLength(12)]],
      RazonSocial: [null, [Validators.required]],
      Celular: [null, [Validators.required, Validators.minLength(9), Validators.maxLength(9)]],
      Email: [null, [Validators.required, Validators.email]]
    });
  }

  getCliente() {
    var controls = this.form.controls;

    this.perfilService.getCliente().subscribe(
      (data: any) => {
        this.arraryCliente = data[0];

        controls.RazonSocial.setValue(this.arraryCliente['RazonSocial']);
        controls.TipoDocumento.setValue(1);
        controls.NroDocumento.setValue(this.arraryCliente['RUC']);
        var telefono = this.arraryCliente['Telefono'] == null ? '' : this.arraryCliente['Telefono'].trim();
        controls.Celular.setValue(telefono);
        controls.Email.setValue(this.arraryCliente['Correo']);
      }
    )
  }

  onSubmit(){
    this.spinner.show();
    const _controls = this.form.controls;

		if (this.form.invalid) {
			Object.keys(_controls).forEach(controlName =>
        _controls[controlName].markAsTouched()
      );

      this.spinner.hide();
    } else if (this.showErrorDate) {
      this.spinner.hide();
    } else {
        this.perfilService.updateCliente(_controls.Email.value, _controls.RazonSocial.value,
        _controls.NroDocumento.value, _controls.Celular.value).subscribe(
          (data: any) => {
            if (data > 0) {
              this.change_ref.markForCheck();
              this.spinner.hide();
              this.toastr.successToastr('Usuario Actualizado.', 'Éxito!', {
                toastTimeout: 2000,
                showCloseButton: true,
                animate: 'fade',
                progressBar: true
              });
            } else {
              this.spinner.hide();
            }
          }, ( errorServicio ) => {
            console.log(errorServicio);
            this.change_ref.markForCheck();
            this.spinner.hide();
          }
        );
    }
  }

  clean_main() {
    this.form.reset();
  }

  onlyNumberKey(event) {
    return (event.charCode == 8 || event.charCode == 0) ? null : (event.charCode >= 48 && event.charCode <= 57);
  }
}
