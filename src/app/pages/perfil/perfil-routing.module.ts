import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CuentaComponent } from './cuenta/cuenta.component';
import { LoggedInGuardService } from 'app/security/guards/logged-in-guard.service';
import { ChangePassComponent } from './change-pass/change-pass.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { HelpComponent } from './help/help.component';

const routes: Routes = [
  {
    path: 'Profile',
    component: CuentaComponent,
    canActivate: [LoggedInGuardService]
  },
  {
    path: 'ChangePass',
    component: ChangePassComponent,
    canActivate: [LoggedInGuardService]
  },
  {
    path: 'Orders',
    component: MyOrdersComponent,
    canActivate: [LoggedInGuardService]
  },
  {
    path: 'MyAccount',
    component: MyAccountComponent,
    canActivate: [LoggedInGuardService]
  },
  {
    path: 'Help',
    component: HelpComponent,
    canActivate: [LoggedInGuardService]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PerfilRoutingModule { }
