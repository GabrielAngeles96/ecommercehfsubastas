import { HostListener, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'app/_core/services/auth.service';
import { StorageService } from 'app/_shared/services/storage.service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: []
})
export class PagesComponent implements OnInit {

  constructor(
    private storage: StorageService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }
}
