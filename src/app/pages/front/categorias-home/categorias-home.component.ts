import { Component, OnInit } from '@angular/core';
import { FrontService } from '../_core/services/front.service';

@Component({
  selector: 'app-categorias-home',
  templateUrl: './categorias-home.component.html',
  styleUrls: ['./categorias-home.component.scss']
})
export class CategoriasHomeComponent implements OnInit {
  arrSuggestions: any[] = [];

  constructor(
    private frontService: FrontService
  ) { }

  ngOnInit() {
  }
}
