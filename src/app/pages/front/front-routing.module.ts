import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CatalogoComponent } from './catalogo/catalogo.component';
import { ProuctDetailComponent } from './catalogo/prouct-detail/prouct-detail.component';
import { LoggedInGuardService } from 'app/security/guards/logged-in-guard.service';
import { ErrorComponent } from '../error/error.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    //canActivate: [LoggedInGuardService]
  },
  {
    path: 'Home',
    component: HomeComponent,
    //canActivate: [LoggedInGuardService]
  },
  {
    path: 'Producto',
    component: ProuctDetailComponent,
    canActivate: [LoggedInGuardService]
  },
  {
    path: 'Producto/:id',
    component: ProuctDetailComponent,
    canActivate: [LoggedInGuardService]
  },
  {
    path: 'Catalogo',
    component: CatalogoComponent,
    canActivate: [LoggedInGuardService],
    children: [
      {
        path:':category_1',
        component: CatalogoComponent,
        children: [
          {
            path:':category_2',
            component: CatalogoComponent,
            children: [
              {
                path:':category_3',
                component: CatalogoComponent,
              }
            ]
          }
        ]
      }
    ]
  },
  {
    path: 'Catalogo/:kw',
    component: CatalogoComponent,
    //canActivate: [LoggedInGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FrontRoutingModule { }
