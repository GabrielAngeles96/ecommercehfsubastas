import { Component, OnInit, ChangeDetectorRef, HostListener } from '@angular/core';
import { NgbRatingConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';
import { ActivatedRoute, Router } from '@angular/router';
import { FrontService } from '../../_core/services/front.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StorageService } from 'app/_shared/services/storage.service';
import { NgxSpinnerModule, NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'app/_core/services/auth.service';
import { Meta } from '@angular/platform-browser';
import { Title } from '@angular/platform-browser';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { JwtService } from 'app/_shared/services/jwt.service';
import { JwtUserModel } from 'app/_shared/models/jwt-user.model';

@Component({
  selector: 'app-prouct-detail',
  templateUrl: './prouct-detail.component.html',
  styleUrls: ['./prouct-detail.component.scss'],
  providers: [ NgbRatingConfig]
})
export class ProuctDetailComponent implements OnInit {
  images = [];//[944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/900/500`);

  image_default = 'https://farm5.staticflickr.com/4363/36346283311_74018f6e7d_o.png';

  form: FormGroup;
  formOferta: FormGroup;

  IdSubasta: number = 0;
  cantidad: number = 1;
  disabled_cant = false;
  showProduct = false;
  showRel = false;

  arraySubasta: any = [];
  arrayFotosSubasta: any = [];
  arrayOfertas: any = [];
  arrTopMat: any[] = [];
  arrBanners: any[] = [];
  arrBtnDynamic: any[] = [];
  objCart = null;

  imgSelected = '';

  time_string = '';

  precioBase: number = 0;

  idPush: string = '';

  jwtUser: JwtUserModel;
  IdUsuario: number = 0;

  FlagBase: boolean = false;
  PrimeraOferta: boolean = false;
  MaxDescuento: number = 0;

  constructor(
    configN: NgbRatingConfig,
    public activatedRoute: ActivatedRoute,
    public frontService: FrontService,
    public router: Router,
    public _formBuilder: FormBuilder,
    public change_ref: ChangeDetectorRef,
    private storageService: StorageService,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
    private authService: AuthService,
    private metaTagService: Meta,
    private titleService: Title,
    private toastr: ToastrManager,
    private afMessaging: AngularFireMessaging,
    private jwtService: JwtService
  ) {
      configN.max = 5;
      configN.readonly = true;
  }

  //#region Browser Update
  browserPrefixes = ['moz', 'ms', 'o', 'webkit'];

  getHiddenPropertyName(prefix) {
    return (prefix ? prefix + 'Hidden' : 'hidden');
  }

  getVisibilityEvent(prefix) {
    return (prefix ? prefix : '') + 'visibilitychange';
  }

  getBrowserPrefix() {
    for (var i = 0; i < this.browserPrefixes.length; i++) {
      if(this.getHiddenPropertyName(this.browserPrefixes[i]) in document) {
        // return vendor prefix
        return this.browserPrefixes[i];
      }
    }

    // no vendor prefix needed
    return null;
  }

  browserPrefix = this.getBrowserPrefix();
  //#endregion

  ngOnInit() {
    this.jwtUser = this.jwtService.getUser();
    this.IdUsuario = this.jwtUser.IdUsuario;
    window.scroll(0,0);
    this.listen();
    this.setTagsAndTitle();
    this.createForm();

    this.activatedRoute.queryParams.subscribe(params => {
      this.IdSubasta = Number(params.id);
      this.requestPermission();
      this.getDataSubata();
    });

    document.addEventListener(
      this.getVisibilityEvent(this.browserPrefix)
      , () => { 
        if (!document.hidden) {
          this.getDataSubata();
        }
      }
    );
  }

  ngAfterViewInit() {
    // var changeFirstImage = setTimeout(() => {
    //   this.forceFirstImage();
    // }, 500)
  }

  ngOnDestroy() {
    var model = {
      prmintSubasta: this.IdSubasta,
      prmstrIdPush: this.idPush
    }
    this.frontService.ChangeStatusIdPushSubasta(model).subscribe(
      (data: any) => {
        // console.log(data)
      }
    )
  }

  requestPermission() {
    this.afMessaging.requestToken.subscribe(
      (token: any) => {
        this.idPush = token;
        var model = {
          prmintSubasta: this.IdSubasta,
          prmstrIdPush: token
        }
        this.frontService.SaveIdPushSubasta(model).subscribe(
          (data: any) => {
            console.log(data)
          }
        )
      }, (error) => {
        console.log(error)
      }
    )
  }

  listen() {
    this.afMessaging.messages.subscribe(
      (message: any) => {
        console.log("listen principal")
        this.arrayOfertas = JSON.parse(message.data.data);
        this.precioBase = Number(this.arrayOfertas[this.arrayOfertas.length - 1].MontoOferta) + 0.1;
        this.formOferta.controls.MontoOferta.setValue(this.precioBase.toFixed(2));
        this.PrimeraOferta = false;
        var itemCliente = [];

        this.arrayOfertas.forEach((ao, pos) => {
          if (ao.Cliente == this.IdUsuario) {
            itemCliente.push(pos)
          }
        })

        setTimeout(function(){
          var indicators = document.getElementsByClassName('step-indicator') as HTMLCollection;
          var labels = document.getElementsByClassName('label') as HTMLCollection;

          for (var i = 0; i < indicators.length; i++) {
            var indicator = indicators.item(i);
            var label = labels.item(i);

            if (itemCliente.find(ic => Number(ic) == Number(i)) != undefined) {              
              indicator.classList.add("oferta-propia");
              label.classList.add("oferta-propia");
            } else {
              indicator.classList.remove("oferta-propia");
              label.classList.remove("oferta-propia");
            }
          }
          // itemCliente.forEach(ic => {
          //   var indicator = indicators.item(ic);
          //   var label = labels.item(ic);
            
          //   indicator.classList.add("oferta-propia");
          //   label.classList.add("oferta-propia");
          // })
        }, 1000);
        this.change_ref.markForCheck();
      }, (error) => {
        console.log(error)
      }
    )
  }

  setTagsAndTitle() {
    this.titleService.setTitle('SUBASTAS HF | PRODUCTO')
      
    //this.metaTagService.updateTag({ name: 'keywords', content: 'Angular SEO Integration, Music CRUD, Angular Universal' });
    this.metaTagService.updateTag({ name: 'robots', content: 'INDEX, FOLLOW' });
    this.metaTagService.updateTag({ name: 'description', content: 'Ingresa y descubre todas las promociones en alimentos frescos y en conserva. ¡Ingresa y haz tu pedido online!' });

    this.metaTagService.updateTag({ property: 'og:title', content: 'SUBASTAS HF' });
    this.metaTagService.updateTag({ property: 'og:type', content: 'website' });
    //this.metaTagService.updateTag({ property: 'og:image', content: 'https://www.bembos.com.pe/bembos/images/logo_1200_x_630.png?v=2' });
    this.metaTagService.updateTag({ property: 'og:url', content: 'https://hortifrut.com/' });
    this.metaTagService.updateTag({ property: 'og:description', content: 'Ingresa y descubre todas las promociones en alimentos frescos y en conserva. ¡Ingresa y haz tu pedido online!' });
  }

  createForm() {
    this.form = this._formBuilder.group({
      Cantidad: [1, [Validators.required]]
    });

    this.formOferta = this._formBuilder.group({
      MontoOferta: ['1.00', [Validators.required]]
    });
  }
  
  getDataSubata() {
    this.frontService.GetDataSubasta(this.IdSubasta).subscribe(
      (data: any[]) => {
        if (data[0].length == 0) {
          this.router.navigateByUrl(this.router.url, { skipLocationChange: true }).then(() => {
            this.router.navigate(['/Home']);
          });
        } else {
          this.showProduct = true;
          this.arraySubasta = data[0][0];
          this.arrayFotosSubasta = data[1];
          this.arrayOfertas = data[2];

          this.FlagBase = this.arraySubasta.FlagBase;
          this.PrimeraOferta = this.arraySubasta.PrimeraOferta;
          this.MaxDescuento = this.arraySubasta.MaxDescuentoPrecio;

          this.formOferta.controls.MontoOferta.setValue(((!this.FlagBase && this.arrayOfertas.length == 0) ? this.arraySubasta.PrecioBase : this.arraySubasta.PrecioBase + 0.1).toFixed(2));

          this.precioBase = (!this.FlagBase && this.arrayOfertas.length == 0) ? this.arraySubasta.PrecioBase : this.arraySubasta.PrecioBase + 0.1;

          this.arrayFotosSubasta.forEach((item, pos) => {
            item.Selected = pos == 0 ? true : false;
          })

          this.imgSelected = this.arrayFotosSubasta[0].UrlImagen;

          var itemCliente = [];

          this.arrayOfertas.forEach((ao, pos) => {
            if (ao.Cliente == this.IdUsuario) {
              itemCliente.push(pos)
            }
          })

          if (this.arraySubasta.Vigente) {
            this.cronometro(this.arraySubasta.FechaSubasta, this.arraySubasta.HoraFin);
          } else if (this.arraySubasta.Cerrado) {
            this.time_string = '00:00:00';
          } else {
            this.time_string = 'HH:mm:ss';
          }
  
          setTimeout(function(){
            var indicators = document.getElementsByClassName('step-indicator') as HTMLCollection;
            var labels = document.getElementsByClassName('label') as HTMLCollection;

            for (var i = 0; i < indicators.length; i++) {
              var indicator = indicators.item(i);
              var label = labels.item(i);

              if (itemCliente.find(ic => Number(ic) == Number(i)) != undefined) {
                indicator.classList.add("oferta-propia");
                label.classList.add("oferta-propia");
              } else {
                indicator.classList.remove("oferta-propia");
                label.classList.remove("oferta-propia");
              }
            }

            // itemCliente.forEach(ic => {
            //   var indicator = indicators.item(ic);
            //   var label = labels.item(ic);
              
            //   indicator.classList.add("oferta-propia");
            //   label.classList.add("oferta-propia");
            // })
          }, 1000);

          this.change_ref.markForCheck();
        }
      }
    );
  }

  flagVenta: boolean = false;
  cronometro(FechaCierre, HoraCierre) {
    var fechaCierreSplit = FechaCierre.split('/');
    var horaCierreSplit = HoraCierre.split(':');

    var date = new Date(fechaCierreSplit[2] + '/' + fechaCierreSplit[1] + '/' + fechaCierreSplit[0]);
    date.setHours(horaCierreSplit[0]);
    date.setMinutes(horaCierreSplit[1]);
    date.setSeconds(0);

    // var date2 = Date.now();

    // var Time = date.getTime() - date2;
    // var time_seconds = Math.round(Time/1000);

    // if (Time > 0) {
      var timer = setInterval(() => {
        var date2 = Date.now();

        var Time = date.getTime() - date2;
        var time_seconds = Math.round(Time/1000);

        // --time_seconds;
        var seconds = '0' + String(time_seconds%60);
        var minutes_ = parseInt(String(time_seconds/60));
        var hours_ = 0;

        if (minutes_ >= 60) {
          hours_ = parseInt(String(minutes_/60));
          minutes_ = minutes_%60;
        }

        var minutes = '0' + String(minutes_);
        var hours = '0' + String(hours_);

        var hours_string = hours.substring(hours.length-2,hours.length);
        var minutes_string = minutes.substring(minutes.length-2,minutes.length);
        var seconds_string = seconds.substring(seconds.length-2,seconds.length);

        
        // console.log(time_seconds)
        if (time_seconds === 0) {
          this.time_string = '00:00:00';
          
          if (!this.flagVenta) {
            this.saveVenta();
            this.flagVenta = true;
          }
          clearInterval(timer);
        } else {
          this.time_string = hours_string + ':' + minutes_string + ':' + seconds_string;
        }
      }, 1000);
    // }
  }

  saveVenta() {
    this.jwtUser = this.jwtService.getUser();

    if (Number(this.arrayOfertas[this.arrayOfertas.length - 1].Cliente) == Number(this.jwtUser.IdUsuario)) {
      this.spinner.show();

      var model = {
        prmintSubasta: this.IdSubasta
      }

      this.frontService.SaveVentaSubasta(model).subscribe(
        (data: any) => {
          if (data) {
            this.toastr.successToastr('La subasta ha finalizado y usted ha sido el ganador.', 'Subasta Ganada!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
            this.spinner.hide();
            setTimeout(() => {
              window.location.reload();
            }, 2000);
          }
        }
      )
    } else {
      this.toastr.warningToastr('La subasta ha finalizado, pero lamentablemente usted no fue el ganador.', 'Subasta Terminada!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
      this.spinner.hide();
      setTimeout(() => {
        window.location.reload();
      }, 2000);
    }
  }

  changeImgSelected(Imagen) {
    this.arrayFotosSubasta.forEach(item => {
      if (item.FotoSubasta == Number(Imagen)) {
        item.Selected = true;
        this.imgSelected = item.UrlImagen;
      } else {
        item.Selected = false;
      }
    })

    this.change_ref;
  }

  formatNumber(n: number){
    var val = n.toFixed(2);
    var parts = val.toString().split(".");
    var num = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");

    return num;
  }

  openModal(content) {
    this.jwtUser = this.jwtService.getUser();
    
    if (this.jwtUser.Bloqueado) {
      this.toastr.errorToastr('Su cuenta está bloqueda, por lo tanto no puede ofertar.', 'Advertencia!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
    }
    else if (!this.arraySubasta.Vigente) {
      if (this.arraySubasta.Cerrado) {
        this.toastr.errorToastr('Esta subasta ya ha finalizado.', 'Advertencia!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      } else {
        this.toastr.errorToastr('Esta subasta aún no ha iniciado.', 'Advertencia!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      }
    } else {
      this.modalService.open(content, { centered: true, windowClass: 'md-confirm' });
    }
  }

  onSubmit() {
    this.spinner.show();

    const _controls = this.formOferta.controls;

		if (this.formOferta.invalid) {
			Object.keys(_controls).forEach(controlName =>
        _controls[controlName].markAsTouched()
      );

      this.spinner.hide();
    } else {
     var model = {
      prmintSubasta: this.IdSubasta,
      prmdecMontoOferta: Number(_controls.MontoOferta.value)
    }

    this.frontService.SaveOfertaSubasta(model).subscribe(
      (data: any) => {
        if (data[0][0].ok) {
          let element: HTMLElement = document.getElementById('close') as HTMLElement;
          element.click();

          this.toastr.successToastr(data[0][0].Message, 'Éxito!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        } else {
          this.toastr.warningToastr(data[0][0].Message, 'Advertencia!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        }

        this.spinner.hide();
      }, (error) => {
        console.log(error)
        this.toastr.warningToastr('Ha ocurrido un error, inténtelo nuevamente más tarde por favor', 'Error!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });

        this.spinner.hide();
      })
    }
}

  clean_main() {
    this.form.reset();
    this.form.controls.Cantidad.setValue(1);
  }

  isExistsCart() {
    if (this.storageService.getJson('Cart')) {
      return true;
    } else {
      return false;
    }
  }

  getUnidades() {
    if (this.cantidad > 1) {
      return this.cantidad + ' unidades';
    } else {
      return this.cantidad + ' unidad';
    }
  }

  increase() {
    var monto = this.formOferta.controls.MontoOferta;
    var stock = 90;

    // if (Number(monto.value) < stock) {
    monto.setValue((Number(monto.value) + 0.10).toFixed(2));
    // } else {
    //   this.toastr.errorToastr('Ya no se cuenta con más stock.', 'Advertencia!', {
    //     toastTimeout: 2000,
    //     showCloseButton: true,
    //     animate: 'fade',
    //     progressBar: true
    //   });
    // }
  }

  decrease() {
    var monto = this.formOferta.controls.MontoOferta;

    if (this.PrimeraOferta && this.FlagBase) {
      if (Number(monto.value) >= this.precioBase*this.MaxDescuento) {
        monto.setValue((Number(monto.value) - 0.10).toFixed(2));
      } else {
        this.toastr.warningToastr('En esta primera oferta solo puede ofrecer hasta un ' + this.MaxDescuento*100 + '% del precio base.', 'Advertencia!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      }
    } else if (Number(monto.value) > this.precioBase) {
      monto.setValue((Number(monto.value) - 0.10).toFixed(2));
    } else {
      this.toastr.warningToastr('No se puede ofertar menos del precio base o monto actual ofertado.', 'Advertencia!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
    }
  }

  changeEventShorcut(Material) {
    var btn = this.arrBtnDynamic.find(b => b.Material == Material);

    if (btn != undefined) {
      return true;
    } else {
      return false;
    }
  }

  prepare_model_shortcut(objMaterial, Cantidad) {
    var pos = [];

    var centro = this.authService.centroSelected;

    if (this.isExistsCart()) {
      this.objCart = this.storageService.getJson('Cart');
      
      pos =  this.objCart.Detalle.filter(function(e) {
        return e.Estado == 1;
      });
    } else {
      this.objCart = {
        Pedido: 0,
        ForPago: 0,
        Centro: centro,
        CantBolsa: 0,
        Descuento: 0,
        Total: 0,
        Usuario: '',
        NroPersona: null,
        TipoUsuario: 0,
        Status: 1,
        Detalle: []
      }
    }

    var last_product = this.objCart.Detalle.find(e => e.Estado == 1 && e.CodMaterial == objMaterial["Codigo"]);//objMaterial["Material"]);

    if (last_product != undefined) {
      var pos_last_product = this.objCart.Detalle.indexOf(last_product);

      this.objCart.Detalle[pos_last_product].Cantidad += Cantidad;
      this.objCart.Detalle[pos_last_product].Estado = (this.objCart.Detalle[pos_last_product].Cantidad == 0) ? 0 : 1;
    } else {
      this.objCart.Detalle.push({
        detaPedido: '',
        Posicion: pos.length + 1,
        CodMaterial: objMaterial["Codigo"],
        Material: objMaterial["Material"],
        DesMat: objMaterial["NombreComercial"],
        Almacen: objMaterial["Almacen"],
        Cantidad: Cantidad,
        UndMed: objMaterial["UnidadMedidaBase"],
        PrecioUnit: objMaterial["Precio"],
        Descuento: 0,
        Moneda: objMaterial["Moneda"],
        Estado: 1
      });
    }

    var subtotal = 0;

    this.objCart.Detalle.forEach(item => {
      if (item.Estado == 1) {
        subtotal += item.Cantidad * item.PrecioUnit;
      }
    })

    this.objCart.Total = subtotal;

    var xml = '<?xml version="1.0" encoding="ISO-8859-1"?><root>';
    
    this.objCart.Detalle.forEach(item => {
      xml += '<Detas detaPedido="'+item.detaPedido+'" Posicion="'+item.Posicion+'" Material="'+item.Material+'" DesMat="'+this.replaceAll(item.DesMat)+'" ';
      xml += 'Almacen="'+item.Almacen+'" Cantidad="'+item.Cantidad+'" UndMed="'+item.UndMed+'" PrecioUnit="'+item.PrecioUnit+'" ';
      xml += 'Descuento="'+item.Descuento+'" Moneda="'+item.Moneda+'" Estado="'+item.Estado+'"/>';
    });
    
    xml += '</root>';

    return {
      Pedido: this.objCart.Pedido,
      ForPago: this.objCart.ForPago,
      Centro: this.objCart.Centro,
      CantBolsa: this.objCart.CantBolsa,
      Descuento: this.objCart.Descuento,
      Total: this.objCart.Total,
      Usuario: this.objCart.Usuario,
      NroPersona: this.objCart.NroPersona,
      TipoUsuario: this.objCart.TipoUsuario,
      Status: this.objCart.Status,
      strXML: xml
    };
  }

  replaceAll(text) {
    let re = /\&/gi;
    return text.replace(re, "-a-n-d-")
  }

  setImage(img) {
    if (img != null) {
      return img;
    } else {
      return this.image_default;
    }
  }
}
