import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { FrontService } from '../_core/services/front.service';
import { JwtUserModel } from 'app/_shared/models/jwt-user.model';
import { JwtService } from 'app/_shared/services/jwt.service';
import { NgbRatingConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Meta } from '@angular/platform-browser';
import { Title } from '@angular/platform-browser';
import { StorageService } from 'app/_shared/services/storage.service';
import { AuthService } from 'app/_core/services/auth.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.scss'],
  providers: [ NgbRatingConfig ]
})
export class CatalogoComponent implements OnInit {
  private sub: any;

  mySubscription: any;

  arrayCategorias: any[];
  arrayVariedades: any[];
  arrayLugares: any[];

  filtersCategoria: any[] = [];
  filtersVariedad: any[] = [];
  filtersLugar: any[] = [];

  arrayCategories: any[];
  arrayCaracteristicas: any[];
  arrayMateriales: any[] = [];
  arrayMaterialesMain: any[] = [];
  arrayOrder: any[] = [{Order: '0', Descripcion: 'Ordenar Por:'}, {Order: '1', Descripcion: 'Mayor a menor precio'},
  {Order: '2', Descripcion: 'Menor a mayor precio'}, {Order: '3', Descripcion: 'Mayor descuento'}];

  category_1 = "";
  category_2 = "";
  category_3 = "";

  jwtUser: JwtUserModel;

  page = 1;

  pageSize = 4;

  orderBy = "0";

  show_not_found = false;

  panelGeneralSelected: any[] = [{PanelGeneral: 'Categoria'}, {PanelGeneral: 'Variedad'}, {PanelGeneral: 'Lugar'}];
  panelCategorySelected: any[] = [];

  mFiltersOpened = false;

  objCart = null;

  keyWord: any;

  activeIds: any[] = ['Categoria', 'Variedad', 'Lugar'];

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public frontService: FrontService,
    public jwtService: JwtService,
    private metaTagService: Meta,
    private titleService: Title,
    private modalService: NgbModal,
    private storageService: StorageService,
    private authService: AuthService,
    private toastr: ToastrManager,
    configN: NgbRatingConfig,
  ) {
    configN.max = 5;
    configN.readonly = true;
    
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    });
  }

  ngOnInit() {
    window.scroll(0,0);
    this.setTagsAndTitle();

    this.jwtUser = this.jwtService.getUser();
    this.arrayCategories = decodeURI(this.router.url).split('/');
    this.arrayCategories.splice(0,2);

    this.getFiltersCatalogo();
    this.category_1 = (this.category_2 == '' && this.category_3 == '') ? this.arrayCategories[0] != undefined ? this.arrayCategories[0] : '' : '';
    this.getCatalogo();

    // this.route.queryParams.subscribe(params => {
    //   this.keyWord = params.kw;
    //   //this.category_3 = this.arrayCategories[2] != undefined ? this.arrayCategories[2] : '';
    //   this.category_2 = (this.category_3 == '') ? this.arrayCategories[1] != undefined ? this.arrayCategories[1] : '' : '';
    //   this.category_1 = (this.category_2 == '' && this.category_3 == '') ? this.arrayCategories[0] != undefined ? this.arrayCategories[0] : '' : '';

    //   if (this.keyWord) {
    //     this.getCatalogoByKeWord();
    //   } else {
    //     this.getCatalogo();
    //   }

    //   this.getFiltersCatalogo();
    // });
  }

  setTagsAndTitle() {
    this.titleService.setTitle('SUBASTAS HF | CATÁLOGO')
      
    //this.metaTagService.updateTag({ name: 'keywords', content: 'Angular SEO Integration, Music CRUD, Angular Universal' });
    this.metaTagService.updateTag({ name: 'robots', content: 'INDEX, FOLLOW' });
    this.metaTagService.updateTag({ name: 'description', content: 'Ingresa y descubre todas las promociones en alimentos frescos y en conserva. ¡Ingresa y haz tu pedido online!' });

    this.metaTagService.updateTag({ property: 'og:title', content: 'SUBASTAS HF' });
    this.metaTagService.updateTag({ property: 'og:type', content: 'website' });
    //this.metaTagService.updateTag({ property: 'og:image', content: 'https://www.bembos.com.pe/bembos/images/logo_1200_x_630.png?v=2' });
    this.metaTagService.updateTag({ property: 'og:url', content: 'https://hortifrut.com/' });
    this.metaTagService.updateTag({ property: 'og:description', content: 'Ingresa y descubre todas las promociones en alimentos frescos y en conserva. ¡Ingresa y haz tu pedido online!' });
  }

  getCatalogo() {
    var model = this.prepare_model();

    this.frontService.GetSubastasByFilters(model).subscribe(
      (data: any) => {
        this.show_not_found = data.length == 0 ? true : false;
        this.arrayMateriales = data;
        this.arrayMaterialesMain = data;
      }
    );
  }

  getFiltersCatalogo() {
    this.frontService.GetFiltersCatalogo().subscribe(
      (data: any) => {
        this.arrayCategorias = data[0];
        this.arrayVariedades = data[1];
        this.arrayLugares = data[2];
      }
    );
  }
  
  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }

  formatNumber(n: number){
    var val = n.toFixed(2);
    var parts = val.toString().split(".");
    var num = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");

    return num;
  }

  prepare_model() {
    var xmlCategoria = '<?xml version="1.0" encoding="ISO-8859-1"?><root>';
    
    if (this.filtersCategoria.length > 0) {
      this.filtersCategoria.forEach(item => {
        xmlCategoria += '<Categoria Valor="' + item + '"/>';
      });
    } else {
      xmlCategoria += '<Categoria Valor="0"/>';
    }
    
    xmlCategoria += '</root>';


    var xmlVariedad = '<?xml version="1.0" encoding="ISO-8859-1"?><root>';

    if (this.filtersVariedad.length > 0) {
      this.filtersVariedad.forEach(item => {
        xmlVariedad += '<Variedad Valor="' + item + '"/>';
      });
    } else {
      xmlVariedad += '<Variedad Valor="0"/>';
    }
    
    xmlVariedad += '</root>';


    var xmlLugar = '<?xml version="1.0" encoding="ISO-8859-1"?><root>';
    
    if (this.filtersLugar.length > 0) {
      this.filtersLugar.forEach(item => {
        xmlLugar += '<Lugar Valor="' + item + '"/>';
      });
    } else {
      xmlLugar += '<Lugar Valor="0"/>';
    }
    
    xmlLugar += '</root>';

    var model = {
      prmstrXmlCategoria: xmlCategoria,
      prmstrXmlVariedad: xmlVariedad,
      prmstrXmlLugar: xmlLugar,
      prmintTipo: this.getTipoSubasta(this.category_1)
    }

    return model;
  }

  getChecks(filter, id) {
    var item = this[filter].find(f => f == id);

    if (item) {
      var index = this[filter].indexOf(item);
      this[filter].splice(index,1);
    } else {
      this[filter].push(id);
    }

    this.getCatalogo();
    // //if (Cat == 'Categoria') {
    // this.router.navigate(['/Catalogo/' + Cat + '/' + Subcat]);
    // //}

    // if (this.mFiltersOpened)  {
    //   let element: HTMLElement = document.getElementById('close') as HTMLElement;
    //   element.click();

    //   this.mFiltersOpened = false;
    // }
  }

  getTipoSubasta(Tipo) {
    if (Tipo == 'Vigentes') {
      return 1;
    } else if (Tipo == 'Proximas') {
      return 2;
    } else if (Tipo == 'Cerradas') {
      return 3;
    } else {
      return 0;
    }
  }

  clearFilters() {
    this.orderBy = '0';

    this.filtersCategoria = [];
    this.filtersVariedad = [];
    this.filtersLugar = [];
    this.getFiltersCatalogo();
    this.getCatalogo();
  }

  orderProducts() {
    if (this.orderBy == '1') {
      this.arrayMateriales.sort((a, b) => {
        var precioA = a["Precio"];
        var precioB = b["Precio"];

        if (precioA < precioB) {
          return 1;
        } else {
          return -1;
        }
      });
    } else if (this.orderBy == '2') {
      this.arrayMateriales.sort((a, b) => {
        var precioA = a["Precio"];
        var precioB = b["Precio"];

        if (precioA > precioB) {
          return 1;
        } else {
          return -1;
        }
      });
    } else if (this.orderBy == '3') {
      this.arrayMateriales.sort((a, b) => {
        var precioA = a["Precio"];
        var precioB = b["Precio"];

        var descA = a["PrecioBase"] - precioA;
        var descB = b["PrecioBase"] - precioB;

        if (descA == descB) {
          if (precioA < precioB) {
            return 1;
          } else {
            return -1;
          }
        } else {
          if (descA < descB) {
            return 1;
          } else{
            return -1;
          }
        }
      });
    } else {
      this.getCatalogo();
    }
  }

  onlyNumberKey(event) {
    return (event.charCode == 8 || event.charCode == 0) ? null : (event.charCode >= 48 && event.charCode <= 57);
  }

  toggleAccordianG(eve) {
    if (eve.nextState) {
      this.panelGeneralSelected.push({PanelGeneral: eve.panelId});
    } else {
      var panelClose = this.panelGeneralSelected.find(p => p.PanelGeneral == eve.panelId);

      this.panelGeneralSelected.splice(this.panelGeneralSelected.indexOf(panelClose), 1);
    }
  }

  toggleAccordianC(eve) {
    if (eve.nextState) {
      this.panelCategorySelected.push({PanelCategory: eve.panelId});
    } else {
      var panelClose = this.panelCategorySelected.find(p => p.PanelCategory == eve.panelId);

      this.panelCategorySelected.splice(this.panelCategorySelected.indexOf(panelClose), 1);
    }
  }

  showIconGeneral(panelId) {
    var panelShow = this.panelGeneralSelected.find(p => p.PanelGeneral == panelId);

    if (panelShow != undefined) {
      return true;
    } else {
      return false;
    }
  }

  showIconCategory(panelId) {
    var panelShow = this.panelCategorySelected.find(p => p.PanelCategory == panelId);

    if (panelShow != undefined) {
      return true;
    } else {
      return false;
    }
  }

  openFilters(content) {
    this.mFiltersOpened = true;
    this.modalService.open(content, { centered: true });
  }

  arrBtnDynamic: any[] = [];

  changeEventShorcut(Material) {
    var btn = this.arrBtnDynamic.find(b => b.Material == Material);

    if (btn != undefined) {
      return true;
    } else {
      return false;
    }
  }

  isExistsCart() {
    if (this.storageService.getJson('Cart')) {
      return true;
    } else {
      return false;
    }
  }

  removeSpaces(text) {
    let re = /\%20/gi;
    return text.replace(/%20/, ' ')
  }

  replaceAll(text) {
    let re = /\&/gi;
    return text.replace(re, "-a-n-d-")
  }

  isSelected(filter, value) {
    var item = this[filter].find(f => f == value);

    if (item) {
      return true;
    } else {
      return false;
    }
  }

  clearArrows() {
    this.panelGeneralSelected = [{PanelGeneral: 'Categoria'}, {PanelGeneral: 'Variedad'}, {PanelGeneral: 'Lugar'}];
  }
}
