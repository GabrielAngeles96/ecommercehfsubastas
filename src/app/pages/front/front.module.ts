import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FrontRoutingModule } from './front-routing.module';
import { HomeComponent } from './home/home.component';
import { BannersComponent } from './banners/banners.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SliderHomeComponent } from './slider-home/slider-home.component';
import { CategoriasHomeComponent } from './categorias-home/categorias-home.component';
import { CatalogoComponent } from './catalogo/catalogo.component';
import { ProuctDetailComponent } from './catalogo/prouct-detail/prouct-detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ArchwizardModule } from 'angular-archwizard';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { PublicidadDialogComponent } from './home/publicidad-dialog/publicidad-dialog.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { RecaptchaModule } from 'ng-recaptcha';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { AngularFireMessagingModule } from '@angular/fire/messaging';

@NgModule({
  declarations: [
    HomeComponent,
    BannersComponent,
    SliderHomeComponent,
    CategoriasHomeComponent,
    CatalogoComponent,
    ProuctDetailComponent,
    PublicidadDialogComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    FrontRoutingModule,
    ArchwizardModule,
    NgxSpinnerModule,
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    NgSelectModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAnL0c0WfKsD8AjIjcNNr-QPSq6UlHVLrQ'
    }),
    GooglePlaceModule,
    RecaptchaModule,
    CarouselModule,
    AngularFireMessagingModule
  ],
  entryComponents: [
    PublicidadDialogComponent
  ]
})
export class FrontModule { }
