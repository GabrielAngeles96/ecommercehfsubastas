import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { FrontService } from '../_core/services/front.service';
import { Router } from '@angular/router';
import { StorageService } from 'app/_shared/services/storage.service';

@Component({
  selector: 'app-banners',
  templateUrl: './banners.component.html',
  styleUrls: ['./banners.component.scss'],
  providers: [NgbCarouselConfig]
})
export class BannersComponent implements OnInit {
  images = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/900/500`);
  route_: string = '/Home';

  arrBannersWeb = [];
  arrBannersApp = [];

  @ViewChild('carousel') carousel: any;
  @ViewChild('carousel2') carousel2: any;

  centroSelected: Number;

  constructor(
    config: NgbCarouselConfig,
    private frontService: FrontService,
    private router: Router,
    private storageService: StorageService,
    private change_ref: ChangeDetectorRef
    ) {
    config.interval = 10000;
    config.wrap = true;
    config.keyboard = false;
    config.pauseOnHover = false;
    config.showNavigationArrows = false;

    // this.arrBannersWeb = this.getBanners();
   }

  ngOnInit() {
    this.centroSelected = this.storageService.get('Centro') ? Number(this.storageService.get('Centro')) : 18;

    this.getBanners();
  }

  getBanners(): any {
    this.frontService.getBannersByPage().subscribe(
      (data: any) => {
        this.arrBannersWeb = data.filter(d => d.Tipo == 'WEB');
        this.arrBannersApp = data.filter(d => d.Tipo == 'APP');
        this.change_ref.markForCheck();

        setTimeout(() => {
          this.carousel.cycle();
          this.carousel2.cycle();
        }, 2000);
      }
    )
  }

  setUrlRedirect(url, urlTR) {
    return decodeURI(url);
  }

  redirectTo(url) {
    this.router.navigateByUrl(this.router.url, { skipLocationChange: true }).then(() => {
      var url_ = url.split('=');

      if (url_.length > 1) {
        this.router.navigate(['/Producto'], { queryParams: { id: url_[1] } });
      } else {
        this.router.navigate([decodeURI(url)]);
      }
    });
  }
}
