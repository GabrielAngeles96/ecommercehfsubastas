import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-publicidad-dialog',
  templateUrl: './publicidad-dialog.component.html',
  styleUrls: ['./publicidad-dialog.component.scss']
})
export class PublicidadDialogComponent implements OnInit {
  public onClose: Subject<boolean>;

  constructor(
    public bsModalRef: BsModalRef,
  ) { }

  ngOnInit() {
    this.onClose = new Subject();
  }

  closeModal() {
    this.onClose.next(true);
    this.bsModalRef.hide();
  }
}
