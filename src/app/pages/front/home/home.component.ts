import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Meta } from '@angular/platform-browser';
import { Title } from '@angular/platform-browser';
import { FrontService } from '../_core/services/front.service';
import { StorageService } from 'app/_shared/services/storage.service';
import { AuthService } from 'app/_core/services/auth.service';
import { NgbRatingConfig } from '@ng-bootstrap/ng-bootstrap';
import { ToastrManager } from 'ng6-toastr-notifications';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  arrayMateriales: any[] = [];
  arrayMaterialesMain: any[] = [];
  arrayPromociones: any[] = [];
  objCart = null;
  modalRef: BsModalRef;
  centroSelected: Number;

  arraySubastasVigentes: any[] = [];
  arraySubastasProximas: any[] = [];
  arraySubastasCerradas: any[] = [];

  isLoggedIn: boolean;

  customOptions: OwlOptions = {
    loop: false,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 4
      },
      940: {
        items: 4
      }
    },
    nav: true,
    
    skip_validateItems: true
  }

  constructor(
    private router: Router,
    private metaTagService: Meta,
    private titleService: Title,
    private frontService: FrontService,
    private storageService: StorageService,
    private authService: AuthService,
    private toastr: ToastrManager,
    private modalService: BsModalService,
    configN: NgbRatingConfig,
  ) {
    configN.max = 5;
    configN.readonly = true;
   }

  ngOnInit() {
    this.centroSelected = Number(this.storageService.get('Centro'));
    this.setTagsAndTitle();
    this.getTopSubastas();
  }

  isLogged() {
    return this.authService.isLoggedIn();
  }

  setTagsAndTitle() {
    this.titleService.setTitle('SUBASTAS HF | HOME')
      
    //this.metaTagService.updateTag({ name: 'keywords', content: 'Angular SEO Integration, Music CRUD, Angular Universal' });
    this.metaTagService.updateTag({ name: 'robots', content: 'INDEX, FOLLOW' });
    this.metaTagService.updateTag({ name: 'description', content: 'Ingresa y descubre todas las promociones en alimentos frescos y en conserva. ¡Ingresa y haz tu pedido online!' });

    this.metaTagService.updateTag({ property: 'og:title', content: 'SUBASTAS HF' });
    this.metaTagService.updateTag({ property: 'og:type', content: 'website' });
    //this.metaTagService.updateTag({ property: 'og:image', content: 'https://www.bembos.com.pe/bembos/images/logo_1200_x_630.png?v=2' });
    this.metaTagService.updateTag({ property: 'og:url', content: 'https://hortifrut.com/' });
    this.metaTagService.updateTag({ property: 'og:description', content: 'Ingresa y descubre todas las promociones en alimentos frescos y en conserva. ¡Ingresa y haz tu pedido online!' });
  }

  getTopSubastas() {
    this.frontService.ListarSubastasByTipo().subscribe(
      (data: any) => {
        this.arrayMaterialesMain = data;
        this.arraySubastasVigentes = data[0];
        this.arraySubastasProximas = data[1];
        this.arraySubastasCerradas = data[2];

        var cont = 1;
        var pos = 0;
        var groupBy = window.innerWidth <= 768 ? 8 : 4;

        for (var i = 0; i < this.arrayMaterialesMain.length; i++) {
          this.arrayMaterialesMain[i].Blocked = 0;

          if (cont == 1) {
              this.arrayMateriales[pos] = [];
              this.arrayMateriales[pos].push(this.arrayMaterialesMain[i]);
              cont++;
          } else if (cont <= groupBy) {
              this.arrayMateriales[pos].push(this.arrayMaterialesMain[i]);

              if (cont == groupBy) {
                cont = 1;
                pos++;
              } else {
                cont++;
              }
          }
        }
      }
    )
  }

  formatNumber(n: number){
    var val = n.toFixed(2);
    var parts = val.toString().split(".");
    var num = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");

    return num;
  }

  arrBtnDynamic: any[] = [];

  changeEventShorcut(Material) {
    var btn = this.arrBtnDynamic.find(b => b.Material == Material);

    if (btn != undefined) {
      return true;
    } else {
      return false;
    }
  }

  isExistsCart() {
    if (this.storageService.getJson('Cart')) {
      return true;
    } else {
      return false;
    }
  }

  replaceAll(text) {
    let re = /\&/gi;
    return text.replace(re, "-a-n-d-")
  }

  setUrlRedirect(url) {
    return decodeURI(url);
  }
}
