import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { FrontService } from '../_core/services/front.service';
import { AuthService } from 'app/_core/services/auth.service';
import { StorageService } from 'app/_shared/services/storage.service';

@Component({
  selector: 'app-slider-home',
  templateUrl: './slider-home.component.html',
  styleUrls: ['./slider-home.component.scss'],
  providers: [NgbCarouselConfig]
})

export class SliderHomeComponent implements OnInit {
  images = [{img: 'https://picsum.photos/id/944/900/500'},{img: 'https://picsum.photos/id/1011/900/500'}, {img: 'https://picsum.photos/id/984/900/500'}, {img: 'https://picsum.photos/id/984/900/500'},{img: 'https://picsum.photos/id/2/900/500'}];
  images_ = [];
  arrCategorias = [];
  arrCategorias_ = [];

  @ViewChild('carousel') carousel: any;

  constructor(
    config: NgbCarouselConfig,
    private authService: AuthService,
    private storageService: StorageService
    ) {
    config.interval = 2000000000;
    config.wrap = true;
    config.keyboard = false;
    config.pauseOnHover = false;
    config.showNavigationArrows = true;
    config.showNavigationIndicators = false;
   }

  ngOnInit() {
    this.getMarcas();

    setTimeout(() => {
      this.carousel.cycle();
    }, 1000);
  }

  getMarcas() {
    var centro = this.storageService.get('Centro') ? this.storageService.get('Centro') : 18;

    this.authService.getCategorias(centro).subscribe(
      (data: any) => {
        // this.arrCategorias_ = data;

        data.forEach(d => {
          if (d.IsHome) {
            this.arrCategorias_.push(d);
          }
        })
        // this.arrCategorias = data;
        // data = data.reverse();

        for (var i = 0; i < data.length; i++) {
          this.arrCategorias[i] = [];
          for (var k = i; k <= i + 7; k++) {
            var new_k = k > (data.length - 1) ? (k - (data.length)) : k;
            this.arrCategorias[i].push(data[new_k]);
          }
        }
        // this.arrCategorias.reverse();
        // this.arrCategorias_.reverse();
      }
    )
  }

  formatText(text) {
    var split = text.split(' ');
    var new_text = '';

    split.forEach((s, p) => {
      s = s.toLowerCase();
      new_text += s.charAt(0).toUpperCase() + s.slice(1);

      if (p + 1 < split.length) {
        new_text += ' ';
      }
    })

    return new_text;
  }
}
