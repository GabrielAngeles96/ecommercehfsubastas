import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { HeaderBasicAuthorizationService } from '../../../../_shared/services/header-basic-authorization.service';
import { ApiEnum } from '../../../../_shared/enums/api.enum';
import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FrontService {

  constructor(
    private http: HttpClient,
    private headerBasicAuthorization: HeaderBasicAuthorizationService
  ) { }

  public GetFiltersCatalogo() {
		return this.http.get(`${environment.api.WS_ECOMMERCE.url}/Subasta/GetFiltersCatalogo`,
		  { headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
		 .pipe(map(data => data));
  }

  public GetSubastasByFilters(model) {
		return this.http.post(`${environment.api.WS_ECOMMERCE.url}/Subasta/GetSubastasByFilters`, model,
		  { headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
		 .pipe(map(data => data));
  }

  public SaveIdPushSubasta(model) {
		return this.http.post(`${environment.api.WS_ECOMMERCE.url}/Subasta/SaveIdPushSubasta`, model,
		  { headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
		 .pipe(map(data => data));
  }

  public ChangeStatusIdPushSubasta(model) {
		return this.http.post(`${environment.api.WS_ECOMMERCE.url}/Subasta/ChangeStatusIdPushSubasta`, model,
		  { headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
		 .pipe(map(data => data));
  }

  public SaveOfertaSubasta(model) {
		return this.http.post(`${environment.api.WS_ECOMMERCE.url}/Subasta/SaveOfertaSubasta`, model,
		  { headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
		 .pipe(map(data => data));
  }

  public SaveVentaSubasta(model) {
		return this.http.post(`${environment.api.WS_ECOMMERCE.url}/Subasta/SaveVentaSubasta`, model,
		  { headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
		 .pipe(map(data => data));
  }

  public GetDataSubasta(Subasta) {
		return this.http.get(`${environment.api.WS_ECOMMERCE.url}/Subasta/GetDataSubasta?prmintSubasta=${Subasta}`,		  
		{ headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
	.pipe(map(data => data));
  }

  public getTopMateriales(CodMaterial, Centro) {
		return this.http.get(`${environment.api.WS_ECOMMERCE.url}/Common/IT_GetMatRelWeb?prmstrCodigo=${CodMaterial}&prmintCentro=${Centro}`,  
		{ headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
	.pipe(map(data => data));
  }

  public getBannersByPage() {
		return this.http.get(`${environment.api.WS_ECOMMERCE.url}/Common/ObtenerBanners`,
		  { headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
		 .pipe(map(data => data));
  }

  public getMarcas() {
		return this.http.get(`${environment.api.WS_ECOMMERCE.url}/Common/MM_GetMarcas`,
		  { headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
		 .pipe(map(data => data));
  }

  public getSuggestions() {
		return this.http.get(`${environment.api.WS_ECOMMERCE.url}/Common/IT_GetSuggestions`,
		  { headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
		 .pipe(map(data => data));
  }

  public ListarSubastasByTipo() {
		return this.http.get(`${environment.api.WS_ECOMMERCE.url}/Subasta/ListarSubastasByTipo`,
		  { headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
		 .pipe(map(data => data));
  }

  public getPromociones() {
	return this.http.get(`${environment.api.WS_ECOMMERCE.url}/Common/IT_GetBannersPromociones`,
	  { headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
	 .pipe(map(data => data));
  }
}