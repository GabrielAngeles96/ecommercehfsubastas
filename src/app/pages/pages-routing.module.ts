import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
//import { ErrorPageComponent } from './snippets/error-page/error-page.component';
import { LoggedInGuardService } from '../security/guards/logged-in-guard.service';
import { NoLoggedInGuardService } from '../security/guards/no-logged-in-guard.service';
import { ErrorComponent } from './error/error.component';

const routes: Routes = [
	{
		path: '',
		component: PagesComponent,
		//canActivate: [LoggedInGuardService],
		children: [
			{
				path: '',
				loadChildren: './front/front.module#FrontModule'
			},
			{
				path: '',
				loadChildren: './perfil/perfil.module#PerfilModule'
			},/*
			// {
			// 	path: 'Seguridad',
			// 	loadChildren: 'app/content/pages/ITModuleApp/ITModuleApp.module#ITModuleAppModule'
			// },*/
		]
	},
	{
		path: '404',
		redirectTo: 'error/404',
		pathMatch: 'full'
	},
	{
		path: 'error/:type',
		component: ErrorComponent
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PagesRoutingModule {}
