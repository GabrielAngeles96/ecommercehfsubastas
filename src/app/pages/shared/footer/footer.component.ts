import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
    test : Date = new Date();
    year = (new Date()).getFullYear();
    url_libro: string = 'https://librodereclamaciones.com.pe/ingresar.php';

    panelGeneralSelected: any[] = [];

    constructor() { }

    ngOnInit() {}

    goLibroReclamaciones() {
        let element: HTMLElement = document.getElementById('btnLibro');
        element.click();
    }

    toggleAccordian(eve) {
        if (eve.nextState) {
          this.panelGeneralSelected.push({PanelGeneral: eve.panelId});
        } else {
          var panelClose = this.panelGeneralSelected.find(p => p.PanelGeneral == eve.panelId);
    
          this.panelGeneralSelected.splice(this.panelGeneralSelected.indexOf(panelClose), 1);
        }
    }

    showIconGeneral(panelId) {
        var panelShow = this.panelGeneralSelected.find(p => p.PanelGeneral == panelId);
    
        if (panelShow != undefined) {
          return true;
        } else {
          return false;
        }
    }
}
