import { Component, OnInit, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { AuthService } from '../../../_core/services/auth.service';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AuthNoticeService } from 'app/_core/services/auth-notice.service';
import { JwtUserModel } from 'app/_shared/models/jwt-user.model';
import { JwtService } from 'app/_shared/services/jwt.service';
import { SessionKeys, TokenStorage } from 'app/_core/services/token-storage.service';
import { NgxSpinnerService } from "ngx-spinner";
import { StorageService } from 'app/_shared/services/storage.service';
import { FrontService } from 'app/pages/front/_core/services/front.service';
import { AuthSocialService } from '../../../_core/services/auth-social.service'
import { NgOption } from '@ng-select/ng-select';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
    private toggleButton: any;
    private sidebarVisible: boolean;

    arrayCat = [];

    arraySubCat = [];

    form: FormGroup;
    formRecovery: FormGroup;

    jwtUser: JwtUserModel;

    isLoggedIn: boolean;

    name_user: string = "";

    newObjCart = null;

    view_error = false;

    arrow_menu = false;

    onlyOneLeft = false;

    leftUser = -105;

    flagLevelOne = false;
    flagLevelTwo = true;
    flagLevelThree = true;
    showRecoveryPass = false;

    category_name_selected = '';
    category_photo_selected = '';

    arraySubCatGrouped: any[] = [];
    products_: any[] = [];

    arrayThirdLevel: any[];
    subCategory_name_selected = "";

    text_error = "";

    constructor(
        public location: Location, private element : ElementRef,
        public authService: AuthService,
        public authNoticeService: AuthNoticeService,
        private modalService: NgbModal,
        public config: NgbModalConfig,
        private router: Router,
        public _formBuilder: FormBuilder,
        public toastr: ToastrManager,
        public changeRef: ChangeDetectorRef,
        public jwtService: JwtService,
        //private _tokenStorage: TokenStorage,
        private spinner: NgxSpinnerService,
        private storage: StorageService,
        private frontService: FrontService,
        // private authSocial: AuthSocialService
    ) {
        this.sidebarVisible = false;
        config.keyboard = false;
        config.backdrop = 'static';
    }

    hideLogin = false;

    ngOnInit() {
        const navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];

        this.createForm();

        // var dataLoged = setInterval(() => {
            this.isLoggedIn = this.authService.isLoggedIn();
            
            if (this.isLoggedIn) {
                this.jwtUser = this.jwtService.getUser();

                if (!this.onlyOneLeft) {
                    // var lengthName = this.jwtUser.Nombres.split(' ')[0].length;
                    var lengthName = this.jwtUser.Nombres.length;

                    this.leftUser += (lengthName*-5) - 20;

                    this.onlyOneLeft = true;
                }

                // this.name_user = '¡Bienvenido(a) ' + this.capitalizeFirstLetter(this.jwtUser.Nombres.split(' ')[0]) + '!';  //this.jwtUser.Nombre + ' ' + this.jwtUser.Apellidos;
                this.name_user = '¡Bienvenido(a) ' + this.jwtUser.Nombres;

                // clearInterval(dataLoged);
            } else {
                let body: HTMLElement = document.getElementById('bodyMain') as HTMLElement;
                body.style.overflow = 'hidden';

                let btnLogin: HTMLElement = document.getElementById('btnLogin') as HTMLElement;
                btnLogin.click();

                this.hideLogin = true;
            }
        // }, 500)
    }

    capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
    }

    createForm() {
        this.form = this._formBuilder.group({
            Email: [null, [Validators.required]],
            Password: [null, [Validators.required]]
        });

        this.formRecovery = this._formBuilder.group({
            Email: [null, [Validators.required]],
            // TipoUsuario: [false],
            // DNI: [null, [Validators.required]]
        });
    }

    default_: any = 17;
    
    replaceAll(text) {
        let re = /\&/gi;
        return text.replace(re, "-a-n-d-")
    }

    openVerticallyCentered(content) {
        this.modalService.open(content, { centered: true, size: 'lg' });
        this.cleanForm();
    }

    login() {
        this.view_error = false;
        this.spinner.show();
        var controls = this.form.controls;
        var centro = Number(this.storage.get('Centro'));

        if (this.form.invalid) {
			Object.keys(controls).forEach(controlName =>
                controls[controlName].markAsTouched()
            );

            this.spinner.hide();
        } else{
            this.authService.login(controls.Email.value, controls.Password.value)
            .subscribe(response => {
                if (response.Ok) {
                    //CARRITO

                    this.jwtUser = this.jwtService.getUser();
                    
                    //CARRITO

                    this.cleanForm();
                    let element: HTMLElement = document.getElementById('close') as HTMLElement;
                    element.click();

                    this.isLoggedIn = this.authService.isLoggedIn();

                    if (!this.onlyOneLeft) {
                        var lengthName = this.jwtUser.Nombres.length;
    
                        this.leftUser += (lengthName*-5) - 10;
    
                        this.onlyOneLeft = true;
                    }
    
                    this.name_user = '¡Bienvenido(a) ' + this.jwtUser.Nombres;

                    if (window.navigator.userAgent.indexOf('Chrome') > 0) {
                        let body: HTMLElement = document.getElementById('bodyMain') as HTMLElement;
                        body.style.overflow = 'overlay';
                    } else {
                        window.location.reload();
                    }

                    this.changeRef.markForCheck();

                    // this.router.navigateByUrl(this.router.url, { skipLocationChange: true }).then(() => {
                    //     var url = this.router.url.split('=');

                    //     if (url.length > 1) {
                    //         if (url[0].split("/")[1] == "Catalogo?kw") {
                    //             this.router.navigate(['/Catalogo'], { queryParams: { kw: url[1] } });
                    //         } else {
                    //             this.router.navigate(['/Producto'], { queryParams: { id: url[1] } });
                    //         }
                    //     } else {                            
                    //         if (this.router.url == '/Register' || this.router.url == '/Cart') {
                    //             this.router.navigate(['/Home']);
                    //         } else{
                    //             this.router.navigate([decodeURI(this.router.url)]);
                    //         }
                    //     }
                    // });

                    // setTimeout(()=> {
                    //     window.location.reload();
                    // },1500)
                } else {
                    this.view_error = true;
                    this.text_error = response.Message;
                }
                this.spinner.hide();
                this.changeRef.detectChanges();
            }, error => {
                this.authNoticeService.setNotice(error.error.Message, 'error');

                this.spinner.hide();
                this.changeRef.detectChanges();
            });
        }
    }

    logout(){
        this.spinner.show();
        setTimeout(() => {
            // if(this.authSocial.verifyLoginSocial()) {
            //     this.authSocial.signOut();
            //     this.authService.isLoggedSocial = false;
            // }

            this.authService.logoff();

            this.name_user = "";

            // this.router.navigateByUrl(this.router.url, { skipLocationChange: true }).then(() => {
            //     var url = this.router.url.split('=');

            //     if (url.length > 1) {
            //         if (url[0].split("/")[1] == "Catalogo?kw") {
            //             this.router.navigate(['/Catalogo'], { queryParams: { kw: url[1] } });
            //         } else {
            //             this.router.navigate(['/Producto'], { queryParams: { id: url[1] } });
            //         }
            //     } else {
            //         if (['/Cart','/Orders','/Profile','/ChangePass'].includes(this.router.url)) {//this.router.url == '/Cart') {
            //             this.router.navigate(['/Home']);
            //         } else {
            //             this.router.navigate([decodeURI(this.router.url)]);
            //         }
            //     }
                
                
            // });

            // this.storage.remove('Cart');
            // this.authService.numberDetails = 0;
            this.hideLogin = true;
            this.isLoggedIn = this.authService.isLoggedIn();
            this.spinner.hide();

            let body: HTMLElement = document.getElementById('bodyMain') as HTMLElement;
            body.style.overflow = 'hidden';

            let btnLogin: HTMLElement = document.getElementById('btnLogin') as HTMLElement;
            btnLogin.click();

            this.authService.redirectToHome();
        },1000)
        // setTimeout(()=> {
        //     window.location.reload();
        // },1500)
        //this.authService.redirectToLogin();
    }

    cleanForm() {
        var controls = this.form.controls;
        this.form.reset();
    }

    cleanFormRecovery() {
        this.formRecovery.reset();
        this.text_success = null;
        this.showRecoveryPass = false;
    }

    filterSecondLevel(firstLevel, name) {
        this.flagLevelOne = true;

        var subcat = this.arrayCat.filter(function(cat) {
            return cat.id == firstLevel;
        });

        subcat.forEach(e => {
            this.arraySubCat = e.Subcategoria;
        })
        
        this.category_name_selected = name;//this.arraySubCat[0]["Categoria"];

        this.flagLevelTwo = false;

        this.products_ = [];

        var products = [];

        if (this.arraySubCat.length > 0) {
            this.arraySubCat.forEach(sub => {
                sub['Producto'].forEach(p => {
                    if (products.length < 8) {
                        products.push(p)
                    }
                })
            })
        }

        this.products_ = products;
        this.changeRef.markForCheck();
    }

    goFirstLevel() {
        this.flagLevelOne = false;
        this.flagLevelTwo = true;
        this.flagLevelThree = true;
    }

    goSecondLevel() {
        this.flagLevelOne = true;
        this.flagLevelTwo = false;
        this.flagLevelThree = true;
    }

    closeModalLogin() {
        this.cleanForm();
        this.cleanFormRecovery();
    }

    activeRecovery(active) {
        this.showRecoveryPass = active;

        if (!active) {
            this.formRecovery.reset();
            this.text_success = null;
        }


        if (this.form.controls['TipoUsuario'].value) {
            this.formRecovery.controls['TipoUsuario'].setValue(true);
            this.setPlaceHolderLoginRecuperar();
        } else {
            this.formRecovery.controls['TipoUsuario'].setValue(false);
            this.setPlaceHolderLoginRecuperar();
        }


    }
    text_success: string;
    RecoveryPass() {
        this.spinner.show();
        var controls = this.formRecovery.controls;

        if (this.formRecovery.invalid) {
			Object.keys(controls).forEach(controlName =>
                controls[controlName].markAsTouched()
            );
            this.spinner.hide();
        } else {
            this.authService.forgotPassword(controls.Email.value/*, controls.DNI.value*/).subscribe(
                (data: any) => {
                    if (data.Ok) {
                        this.text_success = "El correo electrónico se envió con éxito. Por favor, revise su bandeja de entrada."
                        this.formRecovery.reset();
                        this.changeRef.markForCheck();
                        this.spinner.hide();
                    }
                }
            )
        }
    }

    setPlaceHolderLogin() {
        return 'Ingresa tu cuenta de usuario';
    }

    setPlaceHolderLoginRecuperar() {
        return 'Ingresa tu e-mail';
    }
}