import { Injectable } from '@angular/core';
import { AuthService } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";;
import { SocialUser } from "angularx-social-login";

@Injectable({
  providedIn: 'root'
})

export class AuthSocialService {
  public user: SocialUser;
  public loggedIn: boolean;

  constructor(
    private authService: AuthService
  ) { }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }
 
  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  } 
 
  signOut(): void {
    this.authService.signOut();
  }

  verifyLoginSocial() {
    this.authService.authState.subscribe((user) => {
      //console.log(user)
      this.user = user;
      this.loggedIn = (user != null);
    });

    return this.loggedIn;
  }
}
