import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { tap, map } from 'rxjs/operators'
import { HeaderBasicAuthorizationService } from '../../_shared/services/header-basic-authorization.service';
import { JwtService } from '../../_shared/services/jwt.service';
import { StorageService } from '../../_shared/services/storage.service';
import { environment } from '../../../environments/environment';
import { ApiEnum } from '../../_shared/enums/api.enum';
import { StorageKeyEnum } from '../../_shared/enums/storage-key.enum';
import { PermissionNavigationService } from '../../_shared/services/permission-navigation.service';
@Injectable({
    providedIn: 'root'
})
export class AuthService {

    numberDetails = 0;
    isLoggedSocial = false;
    centroSelected = 0;

    constructor(private router: Router,
        private storageService: StorageService,
        private jwtService: JwtService,
        private httpClient: HttpClient,
        private headerBasicAuthorization: HeaderBasicAuthorizationService,
        private permissionNavigationService: PermissionNavigationService) { }

    login(login: string, password: string) {
        return this.httpClient.post(`${environment.api.WS_ECOMMERCE.url}/Seguridad/ZITG_Login`, 
            {
                login: login,
                password: password
            }, {
                headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE)
            }
        )
            .pipe(
                tap((response: any) => {
                    if (response.Ok) {
                        this.storageService.set(StorageKeyEnum.JWT_AUTHORIZATION, response.JWT);
                        this.jwtService.load(response.JWT);
                    }
                })
            )
    }

    logoff() {
        this.permissionNavigationService.reset();
        this.storageService.remove(StorageKeyEnum.JWT_AUTHORIZATION);
        this.jwtService.clear();
    }

    refreshToken() {
        return this.httpClient.get(`${environment.api.WS_ECOMMERCE.url}/Seguridad/refreshToken`, {
            headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE)
        }).pipe(
            tap((response: any) => {
                if (response.Ok) {
                    this.storageService.set(StorageKeyEnum.JWT_AUTHORIZATION, response.JWT);
                    this.jwtService.load(response.JWT);
                }
            })
        );
    }

    getCategorias(Centro) {
		return this.httpClient.get(`${environment.api.WS_ECOMMERCE.url}/Pedido/getCategoriaWeb?prmintCentro=${Centro}`,
		  { headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
		 .pipe(map(data => data));
    }

    forgotPassword(Email: string/*, Dni: string*/) {
        return this.httpClient.post(`${environment.api.WS_ECOMMERCE.url}/Seguridad/RecoveryPass`, 
            {
                Email: Email
                // ,Dni: Dni
            }, {
                headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE)
            }
        )
    }

    resetPassword(Pass: string, RePass: string, Token: string) {
        return this.httpClient.post(`${environment.api.WS_ECOMMERCE.url}/Seguridad/SetNewPassword`, 
            {
                password1: Pass,
                password2: RePass,
                token: Token
            }, {
                headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE)
            }
        )
    }

    isLoggedIn() {
        let jwt: string = this.storageService.get(StorageKeyEnum.JWT_AUTHORIZATION);
        if (jwt != null) {
            this.jwtService.load(jwt);
            return this.jwtService.isValid();
        } else {
            return false;
        }
    }

    redirectToLogin() {
        this.router.navigate(['/signup']);
    }
    redirectToHome() {
        this.router.navigate(['/Home']);
    }

    redirectToMain() {
        this.router.navigate(['/']);
        //this.router.navigate(['/welcome']);
    }

    redirectToNotAllowed() {
        this.router.navigate(['/error/not-allowed']);
    }

    validateSuccessCart(Cart) {
		return this.httpClient.get(`${environment.api.WS_ECOMMERCE.url}/Pedido/EC_ValidateSuccessCart?prmintCart=${Cart}`,
		  { headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
		 .pipe(map(data => data));
    }

    validateStatusCart(Cart) {
		return this.httpClient.get(`${environment.api.WS_ECOMMERCE.url}/Pedido/EC_ValidateStatusCart?prmintCart=${Cart}`,
		  { headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
		 .pipe(map(data => data));
    }

    getCurrentCart() {
		return this.httpClient.post(`${environment.api.WS_ECOMMERCE.url}/Pedido/EC_GetCurrentCart`,{},
		  { headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
		 .pipe(map(data => data));
    }

    getLastSalesOrder() {
		return this.httpClient.post(`${environment.api.WS_ECOMMERCE.url}/Pedido/EC_GetLastSalesOrder`,{},
		  { headers: this.headerBasicAuthorization.get(ApiEnum.WS_ECOMMERCE) })
		 .pipe(map(data => data));
    }
}