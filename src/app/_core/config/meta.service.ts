import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class MetaService {
    public title = "";
    public metaDescription = "";
    public keyWords = "";

    constructor() { }
}