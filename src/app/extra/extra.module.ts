import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExtraRoutingModule } from './extra-routing.module';
import { RecoveryPassComponent } from './content/recovery-pass/recovery-pass.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExtraComponent } from './extra.component';

@NgModule({
  declarations: [
    ExtraComponent,
    RecoveryPassComponent
  ],
  imports: [
    CommonModule,
    ExtraRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class ExtraModule { }
