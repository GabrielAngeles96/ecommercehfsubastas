import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecoveryPassComponent } from './content/recovery-pass/recovery-pass.component';

const routes: Routes = [
  {
    path: 'RecoveryPass',
    component: RecoveryPassComponent,
    //canActivate: [LoggedInGuardService]
  },
  {
    path: 'RecoveryPass:token',
    component: RecoveryPassComponent,
    //canActivate: [LoggedInGuardService]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExtraRoutingModule { }
