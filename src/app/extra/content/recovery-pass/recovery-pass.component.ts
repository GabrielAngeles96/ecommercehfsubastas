import { Component, OnInit, HostBinding, ChangeDetectorRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from 'app/_core/services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { Meta } from '@angular/platform-browser';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-recovery-pass',
  templateUrl: './recovery-pass.component.html',
  styleUrls: ['./recovery-pass.component.scss']
})
export class RecoveryPassComponent implements OnInit {
  @HostBinding('class') classes: string = 'recovery_flex';

  form: FormGroup;

  token: any;
  message: any;

  show_error: boolean = false;
  show_message_error: boolean = false;
  show_message_success: boolean = false;

  constructor(
    private _formBuilder: FormBuilder,
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
    private _changeRef: ChangeDetectorRef,
    private metaTagService: Meta,
    private titleService: Title,
  ) { }

  ngOnInit() {
    this.setTagsAndTitle();
    this.createForm();

    this.activatedRoute.queryParams.subscribe(params => {
      if (params.token) {
        this.token = params.token;
      }
    });
  }

  setTagsAndTitle() {
    this.titleService.setTitle('SUBASTAS HF | RESTABLECER CONTRASEÑA')
      
   //this.metaTagService.updateTag({ name: 'keywords', content: 'Angular SEO Integration, Music CRUD, Angular Universal' });
   this.metaTagService.updateTag({ name: 'robots', content: 'INDEX, FOLLOW' });
   this.metaTagService.updateTag({ name: 'description', content: 'Ingresa y descubre todas las promociones en alimentos frescos y en conserva. ¡Ingresa y haz tu pedido online!' });

   this.metaTagService.updateTag({ property: 'og:title', content: 'SUBASTAS HF' });
   this.metaTagService.updateTag({ property: 'og:type', content: 'website' });
   //this.metaTagService.updateTag({ property: 'og:image', content: 'https://www.bembos.com.pe/bembos/images/logo_1200_x_630.png?v=2' });
   this.metaTagService.updateTag({ property: 'og:url', content: 'https://hortifrut.com/' });
   this.metaTagService.updateTag({ property: 'og:description', content: 'Ingresa y descubre todas las promociones en alimentos frescos y en conserva. ¡Ingresa y haz tu pedido online!' });
  }

  createForm() {
    this.form = this._formBuilder.group({
      NewPass: [null, [Validators.required, Validators.minLength(8)]],
      ReNewPass: [null, [Validators.required, Validators.minLength(8)]],
    })
  }

  resetPass() {
    var controls = this.form.controls;

    this.show_error = false;
    this.show_message_error = false;
    this.show_message_success = false;

    if (this.form.invalid) {
			Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
    } else if (controls.NewPass.value != controls.ReNewPass.value) {
      this.show_error = true;
    } else {
      this.authService.resetPassword(controls.NewPass.value, controls.ReNewPass.value, this.token).subscribe(
        (data: any) => {
          this.message = data[0].Message;

          if (data[0].Ok) {
            this.form.reset();

            this.show_message_success = true;
          } else {
            this.show_message_error = true;
          }

          this._changeRef.markForCheck();
        }
      )
    }
  }

  keyRePass() {
    this.show_error = false;
  }
}
